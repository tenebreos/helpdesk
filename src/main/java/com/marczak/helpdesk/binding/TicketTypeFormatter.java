package com.marczak.helpdesk.binding;

import com.marczak.helpdesk.domain.TicketType;
import com.marczak.helpdesk.services.TicketService;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;
import java.util.Optional;

public class TicketTypeFormatter implements Formatter<TicketType> {

    private final TicketService ticketService;

    public TicketTypeFormatter(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @Override
    public TicketType parse(String text, Locale locale) throws ParseException {
        return ticketService.findTicketTypeById(text).orElseThrow(
                () -> new IllegalArgumentException("TicketType id couldn't be found in the database!")
        );
    }

    @Override
    public String print(TicketType object, Locale locale) {
        return object.getId();
    }
}
