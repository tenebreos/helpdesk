package com.marczak.helpdesk.binding;

import com.marczak.helpdesk.domain.Product;
import com.marczak.helpdesk.repositories.ProductRepository;
import com.marczak.helpdesk.services.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class ProductFormatter implements Formatter<Product> {

    private final ProductService productService;

    public ProductFormatter(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public Product parse(String text, Locale locale) throws ParseException {
        return productService.findById(text).orElseThrow(
                () -> new IllegalArgumentException("Product id couldn't be found in the database.")
        );
    }

    @Override
    public String print(Product object, Locale locale) {
        return object.getId();
    }
}
