package com.marczak.helpdesk.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
public class Ticket {

    public enum State {

        OPENED, IN_PROGRESS, CLOSED;

        public boolean isOpened() {
            return this.equals(OPENED);
        }

        public boolean isInProgress() {
            return this.equals(IN_PROGRESS);
        }

        public boolean isClosed() {
            return this.equals(CLOSED);
        }

        public String getName() {
            String state = this.toString().replace("_", " ");
            String[] words = state.split(" ");
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < words.length; ++i) {
                builder.append(words[i].charAt(0));
                builder.append(words[i].toLowerCase().substring(1));
                if (i != words.length - 1)
                    builder.append(" ");
            }
            return builder.toString();
        }

    }

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    @NotNull
    private Customer customer;

    @ManyToOne
    private Technician technician;

    @ManyToOne
    @NotNull
    private Product product;

    @NotBlank
    private String description;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @NotNull
    private LocalDate timeOpened;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @FutureOrPresent
    private LocalDate timeClosed;

    @NotNull
    @ManyToOne
    private TicketType type;

    @Enumerated(EnumType.STRING)
    private State state = State.OPENED;


    public void accept(Technician technician) {
        if (state == State.OPENED) {
            state = State.IN_PROGRESS;
            this.technician = technician;
        } else {
            throw new IllegalStateException("Attempted to accept a Ticket which wasn't OPENED");
        }
    }

    public void close() {
        if (state == State.IN_PROGRESS) {
            timeClosed = LocalDate.now();
            state = State.CLOSED;
        } else {
            throw new IllegalStateException("Attempted to close a Ticket which wasn't IN_PROGRESS");
        }
    }


    public Ticket(
            @NotNull Customer customer,
            @NotNull Product product,
            @NotBlank String description,
            @NotNull LocalDate timeOpened,
            @NotNull TicketType type) {
        this.customer = customer;
        this.product = product;
        this.description = description;
        this.timeOpened = timeOpened;
        this.type = type;
    }
}