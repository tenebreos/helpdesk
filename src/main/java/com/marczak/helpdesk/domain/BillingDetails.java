package com.marczak.helpdesk.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.CreditCardNumber;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Digits;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
public class BillingDetails {

    @Id @GeneratedValue long id;
    @CreditCardNumber String creditCardNumber;
    @NotBlank @Pattern(regexp = "[0-9]{2}/[0-9]{4}") String expirationDate;
    @Digits(integer = 3, fraction = 0) String cvc;

    public BillingDetails(
            @CreditCardNumber String creditCardNumber,
            String expirationDate,
            @Digits(integer = 3, fraction = 0) String cvc) {
        this.creditCardNumber = creditCardNumber;
        this.expirationDate = expirationDate;
        this.cvc = cvc;
    }
}
