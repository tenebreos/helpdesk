package com.marczak.helpdesk.domain;

import com.marczak.helpdesk.validation.annotations.UniqueTitle;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Set;

import static javax.persistence.FetchType.EAGER;

@Entity
@Data
@NoArgsConstructor
public class Article {

    @Id
    private String urlTitle;
    @ManyToOne @NotNull
    private Technician author;
    @NotBlank @UniqueTitle
    private String title;
    @Lob @NotBlank
    private String text;
    @ManyToMany(fetch = EAGER) @NotEmpty
    private Set<Product> products;

    public void updateUrlTitle() {
        urlTitle = URLEncoder.encode(title.replace(" ", "_").toLowerCase(), StandardCharsets.UTF_8);
    }

    public Article(Technician author, String title, String text, Set<Product> products) {
        this.author = author;
        this.title = title;
        this.text = text;
        this.products = products;
    }
}
