package com.marczak.helpdesk.domain;

import com.marczak.helpdesk.validation.annotations.Iban;
import com.marczak.helpdesk.validation.annotations.ValidName;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
public class Technician {

    @Id @GeneratedValue private long id;
    @Valid @OneToOne private PersonalData personalData;
    @Iban @NotBlank private String bankAccountNumber;
    @PastOrPresent @NotNull private LocalDate hireDate;
    private LocalDate fireDate;

    public boolean isHired() {
        return fireDate == null;
    }

    public Technician(
            @Valid PersonalData personalData,
            @NotBlank String bankAccountNumber,
            @PastOrPresent @NotNull LocalDate hireDate) {
        this.personalData = personalData;
        this.bankAccountNumber = bankAccountNumber;
        this.hireDate = hireDate;
    }
}
