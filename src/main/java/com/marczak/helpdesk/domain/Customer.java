package com.marczak.helpdesk.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.Valid;

@Entity
@Data
@NoArgsConstructor
public class Customer {

    @Id @GeneratedValue private long id;
    @Valid @OneToOne PersonalData personalData;
    @Valid @OneToOne BillingDetails billingDetails;

    public Customer(@Valid PersonalData personalData, @Valid BillingDetails billingDetails) {
        this.personalData = personalData;
        this.billingDetails = billingDetails;
    }
}
