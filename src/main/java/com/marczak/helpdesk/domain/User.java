package com.marczak.helpdesk.domain;

import com.marczak.helpdesk.validation.annotations.UniqueUsername;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Data
@NoArgsConstructor
public class User {

    @Id @GeneratedValue private long id;
    @OneToOne @Valid private Customer customerInfo;
    @OneToOne @Valid private Technician technicianInfo;
    @NotBlank @UniqueUsername private String username;
    @NotEmpty private String password;
    @NotNull Role role = Role.USER;

    public boolean hasRole(Role role) {
        return this.role.equals(role);
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @AssertTrue
    public boolean isTechnicianInfoValid() {
        boolean isTechnician = hasRole(Role.TECHNICIAN);
        return (isTechnician && technicianInfo != null) || (!isTechnician && technicianInfo == null);
    }

    @AssertTrue
    public boolean isCustomerInfoValid() {
        boolean isCustomer = hasRole(Role.USER);
        return (isCustomer && customerInfo != null) || (!isCustomer && customerInfo == null);
    }

    public User(String username, String password, Customer customer) {
        this.customerInfo = customer;
        this.username = username;
        this.password = password;
        this.role = Role.USER;
    }

    public User(@NotBlank String username, @NotEmpty String password, @Valid Technician technician) {
        this.username = username;
        this.password = password;
        this.technicianInfo = technician;
        this.role = Role.TECHNICIAN;
    }
}
