package com.marczak.helpdesk.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


public enum Role {

    ADMIN,
    USER,
    TECHNICIAN;

    public boolean isAdmin() {
        return this.equals(ADMIN);
    }

    public boolean isUser() {
        return this.equals(USER);
    }

    public boolean isTechnician() {
        return this.equals(TECHNICIAN);
    }
}
