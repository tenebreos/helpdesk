package com.marczak.helpdesk.domain;

import com.marczak.helpdesk.validation.annotations.ValidName;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Entity
@Data
@NoArgsConstructor
public class PersonalData {

    @Id @GeneratedValue long id;
    @NotBlank @ValidName String name;
    @NotBlank @ValidName String surname;
    @NotBlank @Email String email;

    public PersonalData(
            @NotBlank String name,
            @NotBlank String surname,
            @NotBlank @Email String email) {
        this.name = name;
        this.surname = surname;
        this.email = email;
    }
}
