package com.marczak.helpdesk.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
@Data
@NoArgsConstructor
public class TicketType {

    @Id @NotBlank String id;
    @NotBlank String name;

    public TicketType(String id, @NotBlank String name) {
        this.id = id;
        this.name = name;
    }
}
