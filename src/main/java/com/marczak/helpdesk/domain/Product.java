package com.marczak.helpdesk.domain;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
@Data
@NoArgsConstructor
public class Product {

    @Id @NotBlank private String id;
    @NotBlank private String name;

    public Product(@NotBlank String id, @NotBlank String name) {
        this.id = id;
        this.name = name;
    }
}
