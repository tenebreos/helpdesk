package com.marczak.helpdesk.validation;

import com.marczak.helpdesk.services.ArticleService;
import com.marczak.helpdesk.validation.annotations.UniqueTitle;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueTitleValidator implements ConstraintValidator<UniqueTitle, String> {

    @Autowired
    private ArticleService articleService;

    @Override
    public void initialize(UniqueTitle constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        // Since the validator is not properly initialised by Hibernate (in the data access layer),
        // the null check allows for not-crashing while validation happens there.
        // Since the articleService field is never initialised, it validates whatever as true,
        // effectively disabling the validator.
        return articleService == null || articleService.findByUrlTitle(value.toLowerCase()).isEmpty();
    }
}
