package com.marczak.helpdesk.validation.annotations;

import com.marczak.helpdesk.validation.IllegalNameValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = IllegalNameValidator.class)
public @interface ValidName {
    String message() default "{ValidName.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

    String[] values() default {};
    boolean ignoreCase() default false;
}
