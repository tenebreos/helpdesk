package com.marczak.helpdesk.validation;

import com.marczak.helpdesk.services.UserService;
import com.marczak.helpdesk.validation.annotations.UniqueUsername;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.NotBlank;

public class UniqueUsernameValidator implements ConstraintValidator<UniqueUsername, String> {

    @Autowired
    private UserService userService;

    @Override
    public void initialize(UniqueUsername constraintAnnotation) {
    }

    @Override
    public boolean isValid(@NotBlank String value, ConstraintValidatorContext context) {
        // Hibernate instantiates validators separately from Spring, and doesn't inject dependencies.
        // If userService isn't injected, we can't tell if the value is valid.
        // Since the business logic of this application is trivial, we can assume that the only
        // two ways for information to enter the database is through HTTP requests and the Initializer Bean.
        // Since HTTP requests are validated correctly and the data in the Initializer is guaranteed to be valid,
        // we can turn this validator off in the data access layer.
        return userService == null || userService.findUserByUsername(value).isEmpty();
    }
}
