package com.marczak.helpdesk.validation;

import com.marczak.helpdesk.validation.annotations.Iban;
import org.iban4j.IbanUtil;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IbanValidator implements ConstraintValidator<Iban, String> {
    @Override
    public void initialize(Iban constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        try {
            IbanUtil.validate(value);
            return true;
        } catch (Exception x) {
            return false;
        }
    }
}
