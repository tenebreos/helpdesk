package com.marczak.helpdesk.validation;

import com.marczak.helpdesk.validation.annotations.ValidName;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IllegalNameValidator implements ConstraintValidator<ValidName, String> {

    private ValidName constraint;

    @Override
    public void initialize(ValidName constraint) {
        this.constraint = constraint;
    }

    @Override
    public boolean isValid(String login, ConstraintValidatorContext constraintValidatorContext) {
        if (login != null) {
            if (!constraint.ignoreCase()) {
                for (int i = 0; i < login.length(); i++) {
                    if (Character.isLowerCase(login.charAt(i)))
                        return true;
                }
            }
        }
        return false;
    }
}
