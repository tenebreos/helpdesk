package com.marczak.helpdesk.controllers;

import com.marczak.helpdesk.controllers.transfer.SearchFilter;
import com.marczak.helpdesk.domain.Role;
import com.marczak.helpdesk.domain.Technician;
import com.marczak.helpdesk.domain.User;
import com.marczak.helpdesk.services.TechnicianService;
import com.marczak.helpdesk.services.TicketService;
import com.marczak.helpdesk.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@Controller
@RequestMapping("/technician")
public class TechnicianController {

    private final UserService userService;
    private final TechnicianService technicianService;
    private final Validator validator;
    private final TicketService ticketService;

    @Autowired
    public TechnicianController(
            UserService userService,
            TechnicianService technicianService,
            TicketService ticketService,
            @Qualifier("mvcValidator") Validator validator
    ) {
        this.userService = userService;
        this.technicianService = technicianService;
        this.ticketService = ticketService;
        this.validator = validator;
    }

    @RequestMapping(method = {GET, POST})
    public ModelAndView index(
            @ModelAttribute("filter") SearchFilter filter,
            @PageableDefault(size = 25) Pageable pageable
    ) {

        ModelAndView modelAndView = new ModelAndView("technician/index");

        Page<Technician> technicianPage;
        if (!filter.isEmpty()) {
            technicianPage = technicianService.findByPhrase(filter.getPhrase(), pageable);
        } else {
            technicianPage = technicianService.findAll(pageable);
        }

        modelAndView.addObject("page", technicianPage);

        return modelAndView;
    }


    @GetMapping("/create")
    public ModelAndView create() {
        ModelAndView modelAndView = new ModelAndView("technician/create");
        modelAndView.addObject("user", new User());
        return modelAndView;
    }

    @PostMapping("/create")
    public String create(User user, String password2, Errors errors) {

        user.setRole(Role.TECHNICIAN);
        user.getTechnicianInfo().setHireDate(LocalDate.now());

        if (!user.getPassword().equals(password2)) {
            errors.reject("register.passwordmismatch", "Passwords do not match!");
        }
        if (password2.isBlank() || password2.isEmpty()) {
            errors.reject("register.passwordblank", "Password must not be blank!");
        }

        validator.validate(user, errors);
        if (errors.hasErrors()) {
            return "technician/create";
        }

        userService.saveUser(user);

        return "redirect:/technician";
    }


    @GetMapping("/delete/{id}")
    public ModelAndView delete(@PathVariable long id) {

        ModelAndView modelAndView = new ModelAndView("technician/delete");
        Technician technician = technicianService.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        if (!technician.isHired()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        modelAndView.addObject("technician", technician);
        return modelAndView;

    }

    @PostMapping("/delete/{id}")
    public String delete_(@PathVariable long id) {
        userService.fire(id);
        return "redirect:/technician";
    }


    @GetMapping("/details/{id}")
    public ModelAndView details(@PathVariable long id) {

        ModelAndView modelAndView = new ModelAndView("technician/details");
        Technician technician = technicianService.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        modelAndView.addObject("technician", technician);
        modelAndView.addObject("ticketsProcessed", ticketService.getAmountProcessedBy(technician));
        return modelAndView;

    }
}
