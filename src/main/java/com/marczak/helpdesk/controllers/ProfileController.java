package com.marczak.helpdesk.controllers;

import com.marczak.helpdesk.domain.Customer;
import com.marczak.helpdesk.domain.Technician;
import com.marczak.helpdesk.domain.User;
import com.marczak.helpdesk.services.ArticleService;
import com.marczak.helpdesk.services.TicketService;
import com.marczak.helpdesk.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
@RequestMapping("/profile")
public class ProfileController {

    private final UserService userService;
    private final ArticleService articleService;
    private final TicketService ticketService;

    @Autowired
    public ProfileController(UserService userService, ArticleService articleService, TicketService ticketService) {
        this.userService = userService;
        this.articleService = articleService;
        this.ticketService = ticketService;
    }

    @GetMapping
    public ModelAndView index(Principal principal) {

        ModelAndView modelAndView = new ModelAndView();
        User user = userService.findUserByUsername(principal.getName()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR));

        if (user.getRole().isTechnician()) {

            Technician technician = user.getTechnicianInfo();
            modelAndView.setViewName("profile/index/technician");
            modelAndView.addObject("technician", technician);
            modelAndView.addObject("amountProcessed", ticketService.getAmountProcessedBy(technician));

        } else if (user.getRole().isUser()) {

            Customer customer = user.getCustomerInfo();
            modelAndView.setViewName("profile/index/customer");
            modelAndView.addObject("customer", customer);
            modelAndView.addObject("amountOpened", ticketService.getAmountOpenedBy(customer));

        }

        assert !user.getRole().isAdmin();

        return modelAndView;
    }
}
