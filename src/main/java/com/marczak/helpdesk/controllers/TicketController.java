package com.marczak.helpdesk.controllers;

import com.marczak.helpdesk.controllers.transfer.SearchFilter;
import com.marczak.helpdesk.domain.*;
import com.marczak.helpdesk.services.ProductService;
import com.marczak.helpdesk.services.TicketService;
import com.marczak.helpdesk.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.time.LocalDate;
import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@Slf4j
@Controller
@RequestMapping("/ticket")
public class TicketController {

    private final ProductService productService;
    private final TicketService ticketService;
    private final UserService userService;
    private final Validator validator;

    @Autowired
    public TicketController(
            ProductService productService,
            TicketService ticketService,
            UserService userService,
            @Qualifier("mvcValidator") Validator validator
    ) {

        this.productService = productService;
        this.ticketService = ticketService;
        this.userService = userService;
        this.validator = validator;
    }


    @RequestMapping(method = {GET, POST})
    public ModelAndView index(
            @ModelAttribute("filter") SearchFilter filter,
            @PageableDefault(size = 25, sort = "product") Pageable pageable,
            Principal principal
    ) {

        ModelAndView modelAndView = new ModelAndView("ticket/index");

        User user = userService.findUserByUsername(principal.getName())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.FORBIDDEN));

        Page<Ticket> page = null;

        if (user.hasRole(Role.TECHNICIAN)) {
            if (filter.isEmpty()) {
                page = ticketService.findTicketByTechnician(user.getTechnicianInfo(), pageable);
            } else {
                page = ticketService.findTicketByPhraseAndTechnician(filter.getPhrase(), user.getTechnicianInfo(), pageable);
            }
        } else if (user.hasRole(Role.USER)) {
            if (filter.isEmpty()) {
                page = ticketService.findTicketByCustomer(user.getCustomerInfo(), pageable);
            } else {
                page = ticketService.findTicketByPhraseAndCustomer(filter.getPhrase(), user.getCustomerInfo(), pageable);
            }
        } else if (user.hasRole(Role.ADMIN)) {
            if (filter.isEmpty()) {
                page = ticketService.findAllTickets(pageable);
            } else {
                page = ticketService.findTicketBySearchPhrase(filter.getPhrase(), pageable);
            }
        }

        assert page != null;

        modelAndView.addObject("page", page);

        return modelAndView;
    }


    @GetMapping("/details/{id}")
    public ModelAndView details(@PathVariable Long id, Principal principal) {

        ModelAndView modelAndView = new ModelAndView();

        Optional<User> userOptional = userService.findUserByUsername(principal.getName());
        assert userOptional.isPresent();
        User user = userOptional.get();

        Optional<Ticket> ticketOptional = ticketService.findTicketById(id);
        if (ticketOptional.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        Ticket ticket = ticketOptional.get();

        if (user.hasRole(Role.USER)) {

            if (!ticket.getCustomer().equals(user.getCustomerInfo())) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN);
            }
            modelAndView.setViewName("ticket/details/user");

        } else if (user.hasRole(Role.TECHNICIAN)) {

            if (!ticket.getState().isOpened() && !ticket.getTechnician().equals(user.getTechnicianInfo())) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN);
            }
            modelAndView.setViewName("ticket/details/technician");

        } else if (user.hasRole(Role.ADMIN)) {

            modelAndView.setViewName("ticket/details/admin");
        }

        modelAndView.addObject(ticket);

        return modelAndView;
    }


    @GetMapping("/create")
    public ModelAndView create() {

        ModelAndView modelAndView = new ModelAndView("ticket/create");
        modelAndView.addObject(new Ticket());
        return modelAndView;

    }

    @PostMapping("/create")
    public String create(Ticket ticket, Errors errors, Principal principal) {

        Customer customer = userService.findUserByUsername(principal.getName()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR)
        ).getCustomerInfo();

        ticket.setCustomer(customer);
        ticket.setTimeOpened(LocalDate.now());

        validator.validate(ticket, errors);

        if (errors.hasErrors()) {
            return "ticket/create";
        }

        ticketService.saveTicket(ticket);

        return "redirect:/ticket";
    }


    @RequestMapping(path = "/accept", method = {GET, POST})  // Write view
    public ModelAndView accept(
            @ModelAttribute("filter") SearchFilter filter,
            @PageableDefault(size = 25, sort = "customer") Pageable pageable,
            Principal principal
    ) {
        ModelAndView modelAndView = new ModelAndView("ticket/accept");

        Technician technician = userService.findUserByUsername(principal.getName()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR)
        ).getTechnicianInfo();

        Page<Ticket> page;
        if (!filter.isEmpty()) {
            page = ticketService.findTicketByStateAndPhrase(filter.getPhrase(), Ticket.State.CLOSED, pageable);
        } else {
            page = ticketService.findTicketByState(Ticket.State.OPENED, pageable);
        }

        modelAndView.addObject("page", page);

        return modelAndView;
    }

    @PostMapping("/accept/{id}")
    public String accept(@PathVariable Long id, Principal principal) {

        Technician technician = userService.findUserByUsername(principal.getName()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR)
        ).getTechnicianInfo();
        Ticket ticket = ticketService.findTicketById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        if (!ticket.getState().equals(Ticket.State.OPENED)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        ticketService.accept(ticket, technician);

        ticketService.saveTicket(ticket);

        return "redirect:/ticket/details/" + id;
    }


    // TODO: add a confirmation page
    @PostMapping("/close/{id}")
    public String close(@PathVariable Long id, Principal principal) {

        Technician technician = userService.findUserByUsername(principal.getName())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR))
                .getTechnicianInfo();

        Ticket ticket = ticketService.findTicketById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        if (!ticket.getTechnician().equals(technician)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        ticketService.close(ticket);

        ticketService.saveTicket(ticket);

        return "redirect:/ticket";
    }


    @ModelAttribute
    public void addFormAttributes(Model model) {
        model.addAttribute("ticketTypes", ticketService.findAllTicketTypes());
        model.addAttribute("products", productService.findAll());
    }

}
