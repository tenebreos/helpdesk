package com.marczak.helpdesk.controllers.interceptors;

import com.marczak.helpdesk.config.DebugProperties;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DebugInterceptor implements HandlerInterceptor {

    public DebugProperties debugProperties;

    public DebugInterceptor(DebugProperties debugProperties) {
        this.debugProperties = debugProperties;
    }

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object handler) throws Exception {
        return true;  // Specifies whether the request should be processed further.
    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response,
                           Object handler,
                           ModelAndView modelAndView) throws Exception {
        if (modelAndView != null && debugProperties.isDebugEnabled()) {
            modelAndView.addObject("debugEnabled", true);
            modelAndView.addObject("printUserRoles", debugProperties.isPrintUserRoles());
            modelAndView.addObject("printSessionAttributes", debugProperties.isPrintSessionAttributes());
            modelAndView.addObject("printModelAttributes", debugProperties.isPrintModelAttributes());
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response,
                                Object handler,
                                Exception ex) throws Exception {

    }
}
