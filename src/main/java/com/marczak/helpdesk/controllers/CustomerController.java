package com.marczak.helpdesk.controllers;

import com.marczak.helpdesk.controllers.transfer.SearchFilter;
import com.marczak.helpdesk.domain.Customer;
import com.marczak.helpdesk.domain.Ticket;
import com.marczak.helpdesk.domain.User;
import com.marczak.helpdesk.services.CustomerService;
import com.marczak.helpdesk.services.TicketService;
import com.marczak.helpdesk.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@Controller
@RequestMapping("/customer")
public class CustomerController {

    private final CustomerService customerService;
    private final UserService userService;
    private final TicketService ticketService;

    @Autowired
    public CustomerController(CustomerService customerService, UserService userService, TicketService ticketService) {
        this.customerService = customerService;
        this.userService = userService;
        this.ticketService = ticketService;
    }

    @RequestMapping(method = {GET, POST})
    public ModelAndView index(
            @ModelAttribute("filter") SearchFilter filter,
            @PageableDefault(size = 25) Pageable pageable
    ) {

        ModelAndView modelAndView = new ModelAndView("customer/index");
        Page<Customer> customerPage;

        if (!filter.isEmpty()) {
            customerPage = customerService.findByPhrase(filter.getPhrase(), pageable);
        } else  {
            customerPage = customerService.findAll(pageable);
        }

        modelAndView.addObject("page", customerPage);

        return modelAndView;
    }

    @GetMapping("/details/{id}")
    public ModelAndView details(@PathVariable long id, Principal principal) {

        ModelAndView modelAndView = new ModelAndView();

        User user = userService.findUserByUsername(principal.getName())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR));
        Customer customer = customerService.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        modelAndView.addObject("customer", customer);
        modelAndView.addObject("ticketsOpened", ticketService.getAmountOpenedBy(customer));

        switch (user.getRole()) {
            case ADMIN:
                modelAndView.setViewName("customer/details/admin");
                break;
            case TECHNICIAN:
                if (!customerService.isCustomerInTechniciansTickets(customer, user.getTechnicianInfo())) {
                    throw new ResponseStatusException(HttpStatus.FORBIDDEN);
                }
                modelAndView.setViewName("customer/details/technician");
                break;
            case USER:
                if (!customer.equals(user.getCustomerInfo())) {
                    throw new ResponseStatusException(HttpStatus.FORBIDDEN);
                }
                modelAndView.setViewName("customer/details/user");
                break;
        }

        return modelAndView;
    }
}
