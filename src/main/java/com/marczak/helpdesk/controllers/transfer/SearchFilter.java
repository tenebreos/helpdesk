package com.marczak.helpdesk.controllers.transfer;

import lombok.Data;

@Data
public class SearchFilter {

    private String phrase;

    public boolean isEmpty() {
        return phrase == null || phrase.isBlank();
    }

}
