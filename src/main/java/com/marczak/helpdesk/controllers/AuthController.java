package com.marczak.helpdesk.controllers;

import com.marczak.helpdesk.domain.Customer;
import com.marczak.helpdesk.domain.Role;
import com.marczak.helpdesk.domain.User;
import com.marczak.helpdesk.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/auth")
public class AuthController {

    private final UserService userService;
    private final Validator validator;

    @Autowired
    public AuthController(
            UserService userService,
            @Qualifier("mvcValidator") Validator validator
   ) {
        this.userService = userService;
        this.validator = validator;
    }

    @GetMapping("/register")
    public ModelAndView register() {

        ModelAndView modelAndView = new ModelAndView("auth/register");
        modelAndView.addObject("user", new User());
        return modelAndView;

    }

    // TODO: perhaps it would be cleaner if we used a DTO?
    @PostMapping("/register")
    public String register(User user, Errors errors, String password2) {

        user.setRole(Role.USER);

        if (!user.getPassword().equals(password2)) {
            errors.reject("register.passwordmismatch", "Passwords do not match!");
        }
        if (password2.isBlank() || password2.isEmpty()) {
            errors.reject("register.passwordblank", "Password must not be blank!");
        }
        validator.validate(user, errors);

        if (errors.hasErrors()) {
            return "auth/register";
        }

        userService.saveUser(user);

        return "redirect:/auth/login";
    }

    @GetMapping("/login")
    public String login() {
        return "auth/login";
    }
}
