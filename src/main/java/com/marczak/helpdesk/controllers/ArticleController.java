package com.marczak.helpdesk.controllers;

import com.marczak.helpdesk.controllers.transfer.SearchFilter;
import com.marczak.helpdesk.domain.Article;
import com.marczak.helpdesk.domain.Role;
import com.marczak.helpdesk.domain.Technician;
import com.marczak.helpdesk.domain.User;
import com.marczak.helpdesk.services.ArticleService;
import com.marczak.helpdesk.services.ProductService;
import com.marczak.helpdesk.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.validation.Validator;

import java.security.Principal;
import java.util.Optional;

@Controller
@RequestMapping("/article")
public class ArticleController {

    private final ArticleService articleService;
    private final UserService userService;
    private final ProductService productService;
    private final Validator validator;


    @Autowired
    public ArticleController(
            ArticleService articleService,
            UserService userService,
            ProductService productService,
            @Qualifier("mvcValidator") Validator validator
    ) {
        this.articleService = articleService;
        this.userService = userService;
        this.productService = productService;
        this.validator = validator;
    }


    @GetMapping
    public ModelAndView index(
            @ModelAttribute("filter") SearchFilter filter, // TODO: test if this will work without an explicit @ModelAttribute
            @PageableDefault(size = 25, sort = "author") Pageable pageable,
            Principal principal
    ) {

        ModelAndView modelAndView = new ModelAndView("article/index");

        Page<Article> articlePage;

        if (filter.isEmpty()) {
            articlePage = articleService.findAll(pageable);
        } else {
            articlePage = articleService.findBySearchPhrase(filter.getPhrase(), pageable);
        }

        if (principal != null) {
            User user = userService.findUserByUsername(principal.getName()).orElseThrow(() -> new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR));
            if (user.getRole().isTechnician()) {
                modelAndView.addObject("technician", user.getTechnicianInfo());
            }
        }

        modelAndView.addObject("page", articlePage);

        return modelAndView;
    }


    @GetMapping(path = "/details/{urlTitle}")
    public ModelAndView details(@PathVariable String urlTitle) {

        ModelAndView modelAndView = new ModelAndView("article/details");

        Optional<Article> article = articleService.findByUrlTitle(urlTitle);

        if (article.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        modelAndView.addObject("article", article.get());

        return modelAndView;
    }


    @GetMapping("/create")
    public ModelAndView create() {

        ModelAndView modelAndView = new ModelAndView("article/create");
        modelAndView.addObject("article", new Article());
        return modelAndView;

    }

    @PostMapping("/create")
    public String create(Article article, Errors errors, Principal principal) {

        Technician author = findArticleAuthor(principal);
        article.setAuthor(author);

        validator.validate(article, errors);

        if (errors.hasErrors()) {
            return "article/create";
        }

        article.updateUrlTitle();

        articleService.save(article);

        return "redirect:/article";
    }


    @GetMapping("/edit/{urlTitle}")
    public ModelAndView edit(@PathVariable String urlTitle, Principal principal) {

        Article article = articleService.findByUrlTitle(urlTitle).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        User user = userService.findUserByUsername(principal.getName()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.FORBIDDEN));

        if (!user.hasRole(Role.TECHNICIAN) || !article.getAuthor().equals(user.getTechnicianInfo())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        ModelAndView modelAndView = new ModelAndView("article/edit");
        modelAndView.addObject("article", article);
        return modelAndView;

    }

    @PostMapping("/edit/{urlTitle}")
    public String edit(Article article, Errors errors) {

        // 1. Find the original article
        // 2. Delete it
        // 3. Update the urlTitle of the new article
        // 4. Save the new article.

        Article originalArticle = articleService.findByUrlTitle(article.getUrlTitle()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        article.setAuthor(originalArticle.getAuthor());

        // TODO: this is an ugly hack. Try solving it with validation groups?
        String title = article.getTitle();
        article.setTitle("a");

        validator.validate(article, errors);

        if (errors.hasErrors()) {
            return "article/edit";
        }

        article.setTitle(title);
        article.updateUrlTitle();
        articleService.delete(originalArticle.getUrlTitle());
        articleService.save(article);

        return "redirect:/article";
    }


    @GetMapping("/delete/{urlTitle}")
    public ModelAndView delete(@PathVariable String urlTitle, Principal principal) {

        ModelAndView modelAndView = new ModelAndView("article/delete");

        if (!isAuthorizedToDelete(urlTitle, principal)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        // TODO: This could be more effective
        Article article = articleService.findByUrlTitle(urlTitle).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        modelAndView.addObject("article", article);

        return modelAndView;
    }

    private boolean isAuthorizedToDelete(@PathVariable String urlTitle, Principal principal) {
        Article article = articleService.findByUrlTitle(urlTitle).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        User user = userService.findUserByUsername(principal.getName()).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR));

        boolean isATechnician = user.hasRole(Role.TECHNICIAN);
        boolean isArticleAuthor = article.getAuthor().equals(user.getTechnicianInfo());

        return isATechnician && isArticleAuthor;
    }

    @PostMapping("/delete/{urlTitle}")
    public String delete_(@PathVariable String urlTitle, Principal principal) {

        if (!isAuthorizedToDelete(urlTitle, principal)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        articleService.delete(urlTitle);

        return "redirect:/article";
    }

    private Technician findArticleAuthor(Principal principal) {
        Optional<User> optionalUser = userService.findUserByUsername(principal.getName());
        assert optionalUser.isPresent() : "Article author isn't in the database!";
        User user = optionalUser.get();
        assert user.hasRole(Role.TECHNICIAN) : "Article author isn't a technician!";
        return optionalUser.get().getTechnicianInfo();
    }

    @ModelAttribute
    public void additionalInfo(Model model) {
        model.addAttribute("products", productService.findAll());
    }
}
