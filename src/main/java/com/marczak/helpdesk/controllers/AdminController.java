package com.marczak.helpdesk.controllers;

import com.marczak.helpdesk.domain.Product;
import com.marczak.helpdesk.domain.TicketType;
import com.marczak.helpdesk.services.ProductService;
import com.marczak.helpdesk.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private final ProductService productService;
    private final TicketService ticketService;

    @Autowired
    public AdminController(ProductService productService, TicketService ticketService) {
        this.productService = productService;
        this.ticketService = ticketService;
    }

    @GetMapping("/create_product")
    public ModelAndView createProduct() {
        ModelAndView modelAndView = new ModelAndView("admin/create_product");
        modelAndView.addObject("product", new Product());
        return modelAndView;
    }

    @PostMapping("/create_product")
    public String createProduct(@Valid Product product, Errors errors) {
        if (errors.hasErrors()) {
            return "admin/create_product";
        }
        productService.save(product);
        return "redirect:/";
    }


    @GetMapping("/create_ticket_type")
    public ModelAndView createTicketType() {
        ModelAndView modelAndView = new ModelAndView("admin/create_ticket_type");
        modelAndView.addObject("ticketType", new TicketType());
        return modelAndView;
    }

    @PostMapping("/create_ticket_type")
    public String createProduct(@Valid TicketType ticketType, Errors errors) {
        if (errors.hasErrors()) {
            return "admin/create_ticket_type";
        }
        ticketService.saveTicketType(ticketType);
        return "redirect:/";
    }

}
