package com.marczak.helpdesk.config;

public class ApplicationProfiles {
    public static final String USERS_IN_DATABASE = "usersInDatabase";
    public static final String USERS_IN_MEMORY = "usersInMemory";
}
