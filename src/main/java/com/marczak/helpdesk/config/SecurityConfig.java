package com.marczak.helpdesk.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;

@EnableWebSecurity
//@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Profile(ApplicationProfiles.USERS_IN_MEMORY)
    public UserDetailsService userDetailsService(PasswordEncoder passwordEncoder) {
        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();

        User.UserBuilder userBuilder = User.builder();
        manager.createUser(userBuilder
                .username("User")
                .password(passwordEncoder.encode("pass"))
                .roles("USER")
                .build()
        );
        manager.createUser(userBuilder
                .username("Admin")
                .password(passwordEncoder.encode("ssap"))
                .roles("ADMIN")
                .build()
        );
        manager.createUser(userBuilder
                .username("SuperUser")  // lol
                .password(passwordEncoder.encode("ssapass"))
                .roles("USER", "ADMIN")
                .build()
        );

        return manager;
    }

    protected void configure(HttpSecurity http) throws Exception {
        http
                .logout(logout -> logout.logoutUrl("/auth/logout").logoutSuccessUrl("/"))
                .formLogin().loginPage("/auth/login").permitAll()
            .and()
                .authorizeRequests()
                .mvcMatchers(
                        "/", "/*.css", "/img/*",
                        "/article/details/*",
                        "/article",
                        "/auth/register"
                ).permitAll()
                .mvcMatchers(
                        "/profile"
                ).hasAnyRole("USER", "TECHNICIAN")
                .mvcMatchers(
                        "/technician/details/*"
                ).hasAnyRole("TECHNICIAN", "ADMIN")
                .mvcMatchers(
                        "/ticket",
                        "/ticket/details/*",
                        "/customer/details/*"
                ).hasAnyRole("USER", "TECHNICIAN", "ADMIN")
                .mvcMatchers(
                        "/ticket/create"
                ).hasRole("USER")
                .mvcMatchers(
                        "/ticket/accept/*",
                        "/ticket/close/*",
                        "/article/create",
                        "/article/edit/*",
                        "/article/delete/*"
                ).hasRole("TECHNICIAN")
                .mvcMatchers(
                        "/technician",
                        "/technician/create",
                        "/technician/delete",
                        "/customer",
                        "/admin/create_product",
                        "/admin/create_ticket_type",
                        "/client"
                ).hasRole("ADMIN")
                .anyRequest().authenticated();
    }

//    private AccessDeniedHandler createAccessDeniedHandler() {
//        AccessDeniedHandlerImpl handler = new AccessDeniedHandlerImpl();
//        handler.setErrorPage("/denied");
//        return handler;
//    }
}
