package com.marczak.helpdesk.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("debug")
@Getter @Setter
public class DebugProperties {
    private boolean debugEnabled = false;
    private boolean printUserRoles = true;
    private boolean printSessionAttributes = false;
    private boolean printModelAttributes = false;
    private boolean initializeDatabase = false;
}
