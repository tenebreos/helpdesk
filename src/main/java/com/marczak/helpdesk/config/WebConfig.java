package com.marczak.helpdesk.config;

import com.marczak.helpdesk.binding.TicketTypeFormatter;
import com.marczak.helpdesk.controllers.interceptors.DebugInterceptor;
import com.marczak.helpdesk.binding.ProductFormatter;
import com.marczak.helpdesk.services.ProductService;
import com.marczak.helpdesk.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    private final DebugProperties debugProperties;
    private final ProductService productService;
    private final TicketService ticketService;

    @Autowired
    public WebConfig(DebugProperties debugProperties,
                     ProductService productService,
                     TicketService ticketService) {
        this.debugProperties = debugProperties;
        this.productService = productService;
        this.ticketService = ticketService;
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addFormatter(new ProductFormatter(productService));
        registry.addFormatter(new TicketTypeFormatter(ticketService));
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("other/index");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new DebugInterceptor(debugProperties));
    }

}
