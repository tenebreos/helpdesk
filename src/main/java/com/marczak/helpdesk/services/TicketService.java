package com.marczak.helpdesk.services;

import com.marczak.helpdesk.domain.Customer;
import com.marczak.helpdesk.domain.Technician;
import com.marczak.helpdesk.domain.Ticket;
import com.marczak.helpdesk.domain.TicketType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface TicketService {

    Optional<Ticket> findTicketById(Long id);
    Page<Ticket> findTicketByTechnician(Technician technicianInfo, Pageable pageable);
    Page<Ticket> findTicketByPhraseAndTechnician(String phrase, Technician technicianInfo, Pageable pageable);
    Page<Ticket> findTicketByCustomer(Customer customerInfo, Pageable pageable);
    Page<Ticket> findTicketByPhraseAndCustomer(String phrase, Customer customerInfo, Pageable pageable);
    Page<Ticket> findTicketBySearchPhrase(String phrase, Pageable pageable);
    Page<Ticket> findTicketByStateAndPhrase(String phrase, Ticket.State closed, Pageable pageable);
    Page<Ticket> findTicketByState(Ticket.State opened, Pageable pageable);
    Page<Ticket> findAllTickets(Pageable pageable);

    void saveTicket(Ticket ticket);
    void saveTicketType(TicketType ticketType);

    Optional<TicketType> findTicketTypeById(String id);
    List<TicketType> findAllTicketTypes();

    void accept(Ticket ticket, Technician technician);
    void close(Ticket ticket);

    int getAmountProcessedBy(Technician technician);
    int getAmountOpenedBy(Customer customer);

}
