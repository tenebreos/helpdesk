package com.marczak.helpdesk.services;

import com.marczak.helpdesk.domain.Customer;
import com.marczak.helpdesk.domain.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Optional;

public interface UserService extends UserDetailsService {
    Optional<User> findUserByUsername(String username);
    void saveUser(User user);
    String encodePassword(String plaintext);
    void fire(long id);
}
