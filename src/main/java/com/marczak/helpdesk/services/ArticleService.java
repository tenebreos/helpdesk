package com.marczak.helpdesk.services;

import com.marczak.helpdesk.domain.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface ArticleService {
    Page<Article> findAll(Pageable pageable);
    Page<Article> findBySearchPhrase(String phrase, Pageable pageable);
    Optional<Article> findByUrlTitle(String title);
    void save(Article article);
    void delete(String urlTitle);
}
