package com.marczak.helpdesk.services;

import com.marczak.helpdesk.domain.Technician;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface TechnicianService {

    Page<Technician> findByPhrase(String phrase, Pageable pageable);
    Page<Technician> findAll(Pageable pageable);
    Optional<Technician> findById(long id);

}
