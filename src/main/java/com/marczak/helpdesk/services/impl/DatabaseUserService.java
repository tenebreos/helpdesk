package com.marczak.helpdesk.services.impl;

import com.marczak.helpdesk.config.ApplicationProfiles;
import com.marczak.helpdesk.domain.Role;
import com.marczak.helpdesk.domain.User;
import com.marczak.helpdesk.repositories.*;
import com.marczak.helpdesk.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service("userDetailsService")
@Profile(ApplicationProfiles.USERS_IN_DATABASE)
public class DatabaseUserService implements UserService {

    private final UserRepository userRepository;
    private final CustomerRepository customerRepository;
    private final TechnicianRepository technicianRepository;
    private final PasswordEncoder passwordEncoder;
    private final PersonalDataRepository personalDataRepository;
    private final BillingDetailsRepository billingDetailsRepository;

    @Autowired
    public DatabaseUserService(UserRepository userRepository,
                               CustomerRepository customerRepository,
                               TechnicianRepository technicianRepository,
                               PersonalDataRepository personalDataRepository,
                               BillingDetailsRepository billingDetailsRepository,
                               PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.customerRepository = customerRepository;
        this.technicianRepository = technicianRepository;
        this.personalDataRepository = personalDataRepository;
        this.billingDetailsRepository = billingDetailsRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isEmpty()) {
            throw new UsernameNotFoundException(username);
        }
        return convertToUserDetails(user.get());
    }

    private UserDetails convertToUserDetails(User user) {
        Set<GrantedAuthority> grantedAuthoritySet = new HashSet<>();
        grantedAuthoritySet.add(new SimpleGrantedAuthority("ROLE_" + user.getRole().toString()));
        boolean isNonExpired = true;
        if (user.getRole().isTechnician()) {
            isNonExpired = user.getTechnicianInfo().isHired();
        }
        return new org.springframework.security.core.userdetails.User(
                user.getUsername(), user.getPassword(),
                true, isNonExpired,
                true, true,
                grantedAuthoritySet);
    }

    @Override
    public Optional<User> findUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    @Transactional
    public void saveUser(User user) {

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        if (user.hasRole(Role.TECHNICIAN)) {

            assert !user.hasRole(Role.USER) && !user.hasRole(Role.ADMIN);
            personalDataRepository.save(user.getTechnicianInfo().getPersonalData());
            technicianRepository.save(user.getTechnicianInfo());

        } else if (user.hasRole(Role.USER)) {

            assert !user.hasRole(Role.TECHNICIAN) && !user.hasRole(Role.ADMIN);
            personalDataRepository.save(user.getCustomerInfo().getPersonalData());
            billingDetailsRepository.save(user.getCustomerInfo().getBillingDetails());
            customerRepository.save(user.getCustomerInfo());

        }

        userRepository.save(user);
    }

    @Override
    public String encodePassword(String plaintext) {
        return passwordEncoder.encode(plaintext);
    }

    @Override
    public void fire(long id) {
        technicianRepository.findById(id).ifPresent(technician -> {
            technician.setFireDate(LocalDate.now());
            technicianRepository.save(technician);
        });
    }
}
