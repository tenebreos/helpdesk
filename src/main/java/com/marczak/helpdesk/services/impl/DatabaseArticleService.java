package com.marczak.helpdesk.services.impl;

import com.marczak.helpdesk.domain.Article;
import com.marczak.helpdesk.repositories.ArticleRepository;
import com.marczak.helpdesk.services.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DatabaseArticleService implements ArticleService {

    private final ArticleRepository articleRepository;

    @Autowired
    public DatabaseArticleService(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @Override
    public Page<Article> findBySearchPhrase(String phrase, Pageable pageable) {
        return articleRepository.findBySearchString(phrase, pageable);
    }

    @Override
    public Optional<Article> findByUrlTitle(String urlTitle) {
        return articleRepository.findById(urlTitle);
    }

    @Override
    public Page<Article> findAll(Pageable pageable) {
        return articleRepository.findAll(pageable);
    }

    @Override
    public void save(Article article) {
        articleRepository.save(article);
    }

    @Override
    public void delete(String urlTitle) {
        articleRepository.deleteById(urlTitle);
    }
}
