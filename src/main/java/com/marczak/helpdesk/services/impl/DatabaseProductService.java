package com.marczak.helpdesk.services.impl;

import com.marczak.helpdesk.domain.Product;
import com.marczak.helpdesk.repositories.ProductRepository;
import com.marczak.helpdesk.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DatabaseProductService implements ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public DatabaseProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public Optional<Product> findById(String id) {
        return productRepository.findById(id);
    }

    @Override
    public void save(Product product) {
        productRepository.save(product);
    }
}
