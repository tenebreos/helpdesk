package com.marczak.helpdesk.services.impl;

import com.marczak.helpdesk.services.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class EmailServiceImpl implements EmailService {

    private static final String ADDRESS = "noreply@hytio.com";

    private final JavaMailSender mailSender;
    private final TemplateEngine templateEngine;

    @Autowired
    public EmailServiceImpl(
            JavaMailSender mailSender,
            TemplateEngine templateEngine
    ) {
        this.mailSender = mailSender;
        this.templateEngine = templateEngine;
    }

    @Override
    public void sendEmail(String to, String subject, String title, String text) {
        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, "utf-8");
            helper.setFrom(ADDRESS);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(formatMessage(title, text), true);
            // TODO: test
//            mailSender.send(message);
        } catch (MessagingException ignore) {}
    }

    private String formatMessage(String title, String body) {
        Context context = new Context();
        context.setVariable("title", title);
        context.setVariable("body", body);
        return templateEngine.process("other/email/notification", context);
    }
}
