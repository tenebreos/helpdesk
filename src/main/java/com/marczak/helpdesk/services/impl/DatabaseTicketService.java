package com.marczak.helpdesk.services.impl;

import com.marczak.helpdesk.domain.Customer;
import com.marczak.helpdesk.domain.Technician;
import com.marczak.helpdesk.domain.Ticket;
import com.marczak.helpdesk.domain.TicketType;
import com.marczak.helpdesk.repositories.TicketRepository;
import com.marczak.helpdesk.repositories.TicketTypeRepository;
import com.marczak.helpdesk.services.EmailService;
import com.marczak.helpdesk.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DatabaseTicketService implements TicketService {

    private final TicketRepository ticketRepository;
    private final TicketTypeRepository ticketTypeRepository;
    private final EmailService emailService;

    @Autowired
    public DatabaseTicketService(
            TicketRepository ticketRepository,
            TicketTypeRepository ticketTypeRepository,
            EmailService emailService
    ) {
        this.ticketRepository = ticketRepository;
        this.ticketTypeRepository = ticketTypeRepository;
        this.emailService = emailService;
    }

    @Override
    public Page<Ticket> findTicketByTechnician(Technician technician, Pageable pageable) {
        return ticketRepository.findByTechnician(technician, pageable);
    }

    @Override
    public Page<Ticket> findTicketByPhraseAndTechnician(String phrase, Technician technician, Pageable pageable) {
        return ticketRepository.findByPhraseAndTechnician(phrase, technician, pageable);
    }

    @Override
    public Page<Ticket> findTicketByCustomer(Customer customer, Pageable pageable) {
        return ticketRepository.findByCustomer(customer, pageable);
    }

    @Override
    public Page<Ticket> findTicketByPhraseAndCustomer(String phrase, Customer customer, Pageable pageable) {
        return ticketRepository.findByPhraseAndCustomer(phrase, customer, pageable);
    }

    @Override
    public Optional<Ticket> findTicketById(Long id) {
        return ticketRepository.findById(id);
    }

    @Override
    public Page<Ticket> findAllTickets(Pageable pageable) {
        return ticketRepository.findAll(pageable);
    }

    @Override
    public Page<Ticket> findTicketBySearchPhrase(String phrase, Pageable pageable) {
        return ticketRepository.findBySearchPhrase(phrase, pageable);
    }

    @Override
    public Page<Ticket> findTicketByStateAndPhrase(String phrase, Ticket.State closed, Pageable pageable) {
        return ticketRepository.findByStateAndPhrase(phrase, closed, pageable);
    }

    @Override
    public Page<Ticket> findTicketByState(Ticket.State opened, Pageable pageable) {
        return ticketRepository.findByState(opened, pageable);
    }

    @Override
    public List<TicketType> findAllTicketTypes() {
        return ticketTypeRepository.findAll();
    }

    @Override
    public Optional<TicketType> findTicketTypeById(String id) {
        return ticketTypeRepository.findById(id);
    }

    @Override
    public int getAmountProcessedBy(Technician technician) {
        return ticketRepository.findByTechnicianAndState(technician, Ticket.State.CLOSED).size();
    }

    @Override
    public int getAmountOpenedBy(Customer customer) {
        return ticketRepository.findByCustomer(customer).size();
    }

    @Override
    public void saveTicket(Ticket ticket) {
        ticketRepository.save(ticket);
    }

    @Override
    public void saveTicketType(TicketType ticketType) {
        ticketTypeRepository.save(ticketType);
    }

    @Override
    public void accept(Ticket ticket, Technician technician) {
        ticket.accept(technician);
        String email = ticket.getCustomer().getPersonalData().getEmail();
        String subject = "Your ticket has been accepted.";
        String heading = "Ticket state notification";
        String body = String.format("Your ticket regarding %s has been accepted by %s %s!",
                ticket.getProduct().getName(),
                technician.getPersonalData().getName(),
                technician.getPersonalData().getSurname());
        emailService.sendEmail(email, subject, heading, body);
    }

    @Override
    public void close(Ticket ticket) {
        ticket.close();
        String email = ticket.getCustomer().getPersonalData().getEmail();
        String subject = "Your ticket has been closed.";
        String heading = "Ticket state notification";
        String body = String.format("Your ticket regarding %s has been closed by %s %s.",
                ticket.getProduct().getName(),
                ticket.getTechnician().getPersonalData().getName(),
                ticket.getTechnician().getPersonalData().getName());
        emailService.sendEmail(email, subject, heading, body);
    }
}
