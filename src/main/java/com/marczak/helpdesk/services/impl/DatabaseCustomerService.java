package com.marczak.helpdesk.services.impl;

import com.marczak.helpdesk.domain.Customer;
import com.marczak.helpdesk.domain.Technician;
import com.marczak.helpdesk.repositories.CustomerRepository;
import com.marczak.helpdesk.repositories.TicketRepository;
import com.marczak.helpdesk.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DatabaseCustomerService implements CustomerService {

    private final CustomerRepository customerRepository;
    private final TicketRepository ticketRepository;

    @Autowired
    public DatabaseCustomerService(CustomerRepository customerRepository, TicketRepository ticketRepository) {
        this.customerRepository = customerRepository;
        this.ticketRepository = ticketRepository;
    }

    @Override
    public Page<Customer> findByPhrase(String phrase, Pageable pageable) {
        return customerRepository.findByPhrase(phrase, pageable);
    }

    @Override
    public Page<Customer> findAll(Pageable pageable) {
        return customerRepository.findAll(pageable);
    }

    @Override
    public Optional<Customer> findById(long id) {
        return customerRepository.findById(id);
    }

    @Override
    public boolean isCustomerInTechniciansTickets(Customer customer, Technician technicianInfo) {
        return ticketRepository.findByTechnicianAndCustomer(technicianInfo, customer).size() != 0;
    }
}
