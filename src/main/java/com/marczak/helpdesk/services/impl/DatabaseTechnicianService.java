package com.marczak.helpdesk.services.impl;

import com.marczak.helpdesk.domain.Technician;
import com.marczak.helpdesk.repositories.TechnicianRepository;
import com.marczak.helpdesk.services.TechnicianService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DatabaseTechnicianService implements TechnicianService {

    private final TechnicianRepository technicianRepository;

    @Autowired
    public DatabaseTechnicianService(TechnicianRepository technicianRepository) {
        this.technicianRepository = technicianRepository;
    }

    @Override
    public Page<Technician> findByPhrase(String phrase, Pageable pageable) {
        return technicianRepository.findByPhrase(phrase, pageable);
    }

    @Override
    public Page<Technician> findAll(Pageable pageable) {
        return technicianRepository.findAll(pageable);
    }

    @Override
    public Optional<Technician> findById(long id) {
        return technicianRepository.findById(id);
    }
}
