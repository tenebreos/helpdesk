package com.marczak.helpdesk.services;

import com.marczak.helpdesk.domain.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    List<Product> findAll();
    Optional<Product> findById(String id);
    void save(Product product);
}
