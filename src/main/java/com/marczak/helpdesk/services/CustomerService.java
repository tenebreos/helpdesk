package com.marczak.helpdesk.services;

import com.marczak.helpdesk.domain.Customer;
import com.marczak.helpdesk.domain.Technician;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface CustomerService {

    Page<Customer> findByPhrase(String phrase, Pageable pageable);
    Page<Customer> findAll(Pageable pageable);
    Optional<Customer> findById(long id);
    boolean isCustomerInTechniciansTickets(Customer customer, Technician technicianInfo);

}
