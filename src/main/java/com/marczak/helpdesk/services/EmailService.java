package com.marczak.helpdesk.services;

public interface EmailService {
    void sendEmail(String to, String subject, String title, String text);
}
