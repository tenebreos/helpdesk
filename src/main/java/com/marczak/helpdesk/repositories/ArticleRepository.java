package com.marczak.helpdesk.repositories;

import com.marczak.helpdesk.domain.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ArticleRepository extends JpaRepository<Article, String> {
    @Query("select a from Article as a " +
            "inner join Product as p on p.name like concat('%', ?1, '%') " +
            "where " +
            "a.author.personalData.name like concat('%', ?1, '%') or " +
            "a.author.personalData.surname like concat('%', ?1, '%') or " +
            "a.title like concat('%', ?1, '%') or " +
            "p member of a.products"
    )
    Page<Article> findBySearchString(String phrase, Pageable pageable);
}
