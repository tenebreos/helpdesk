package com.marczak.helpdesk.repositories;

import com.marczak.helpdesk.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {
}
