package com.marczak.helpdesk.repositories;

import com.marczak.helpdesk.config.DebugProperties;
import com.marczak.helpdesk.domain.*;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDate;
import java.util.*;

@Configuration
public class Initializer {

    private final boolean shouldInitialize;
    private final CustomerRepository customerRepository;
    private final ProductRepository productRepository;
    private final TicketRepository ticketRepository;
    private final ArticleRepository articleRepository;
    private final TechnicianRepository technicianRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final TicketTypeRepository ticketTypeRepository;
    private final PersonalDataRepository personalDataRepository;
    private final BillingDetailsRepository billingDetailsRepository;

    @Autowired
    public Initializer(DebugProperties debugProperties,
                       CustomerRepository customerRepository,
                       ProductRepository productRepository,
                       TicketRepository ticketRepository,
                       TicketTypeRepository ticketTypeRepository,
                       ArticleRepository articleRepository,
                       TechnicianRepository technicianRepository,
                       PersonalDataRepository personalDataRepository,
                       UserRepository userRepository,
                       BillingDetailsRepository billingDetailsRepository,
                       PasswordEncoder passwordEncoder) {
        this.shouldInitialize = debugProperties.isInitializeDatabase();
        this.customerRepository = customerRepository;
        this.productRepository = productRepository;
        this.ticketRepository = ticketRepository;
        this.ticketTypeRepository = ticketTypeRepository;
        this.articleRepository = articleRepository;
        this.technicianRepository = technicianRepository;
        this.userRepository = userRepository;
        this.personalDataRepository = personalDataRepository;
        this.passwordEncoder = passwordEncoder;
        this.billingDetailsRepository = billingDetailsRepository;
    }

    @Bean
    InitializingBean init() {
        return () -> {
            if (shouldInitialize) {

                // Initialize product and ticket type data
                productRepository.save(new Product("WIN10", "Windows 10"));
                productRepository.save(new Product("LNX", "Linux Distributions"));
                productRepository.save(new Product("JAVA", "Java"));
                productRepository.save(new Product("INJID", "IntelliJ Idea"));
                productRepository.save(new Product("MTLB", "Matlab"));
                productRepository.save(new Product("VIM", "Vim"));

                ticketTypeRepository.save(new TicketType("NR", "Normal"));
                ticketTypeRepository.save(new TicketType("HI", "High Priority"));
                ticketTypeRepository.save(new TicketType("ON", "On Premises"));


                // Initialize users
                PersonalData personalData;
                User user;

                personalData = new PersonalData("Chris", "Moore", "chris@hytio.com");
                Technician technician = new Technician(personalData, "NL06ABNA7905595609", LocalDate.now());
                user = new User("chris", passwordEncoder.encode("moore"), technician);
                personalDataRepository.save(personalData);
                technicianRepository.save(technician);
                userRepository.save(user);

                personalData = new PersonalData("Andrew", "Morawski", "andrewm@asdf.com");
                BillingDetails billingDetails = new BillingDetails(
                        "5270130922214116",
                        "09/2035",
                        "108"
                );
                Customer customer = new Customer(personalData, billingDetails);
                user = new User("amor", passwordEncoder.encode("andski"), customer);
                personalDataRepository.save(personalData);
                billingDetailsRepository.save(billingDetails);
                customerRepository.save(customer);
                userRepository.save(user);

                user = new User();
                user.setUsername("nimda");
                user.setPassword(passwordEncoder.encode("321oslah"));
                user.setRole(Role.ADMIN);
                userRepository.save(user);


                // Initialize articles
                Article article;
                article = new Article(technician,
                        "Best Vim Color Schemes",
                        "1. Gruvbox\n2. VSDark\n3. Onehalf\n4. Solarized",
                        new HashSet<Product>(Collections.singletonList(
                                productRepository.findById("VIM").get()
                        )));
                article.updateUrlTitle();
                articleRepository.save(article);

                article = new Article(technician,
                        "How to shut down a Windows PC from cmd",
                        "Use\n\tshutdown /s /t 0\nIt's that easy.",
                        new HashSet<Product>(Collections.singletonList(
                                productRepository.findById("WIN10").get()
                        )));
                article.updateUrlTitle();
                articleRepository.save(article);


                // Initialize tickets
                ticketRepository.save(new Ticket(customer,
                        productRepository.findById("LNX").get(),
                        "I moved a root partition to a different drive. After assigning a new id to the copy and " +
                                "modifying /etc/fstab, system still boots off of the old partition",
                        LocalDate.now(),
                        ticketTypeRepository.findById("NR").get()
                ));

                ticketRepository.save(new Ticket(customer,
                        productRepository.findById("Vim").get(),
                        "I typed 'vim' into a terminal, and my keyboard ESC button is broken. What should I do to exit?",
                        LocalDate.now(),
                        ticketTypeRepository.findById("HI").get()
                ));
            }
        };
    }
}
