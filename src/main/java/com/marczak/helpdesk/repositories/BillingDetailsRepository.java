package com.marczak.helpdesk.repositories;

import com.marczak.helpdesk.domain.BillingDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BillingDetailsRepository extends JpaRepository<BillingDetails, Long> {
}
