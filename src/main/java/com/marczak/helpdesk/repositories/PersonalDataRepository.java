package com.marczak.helpdesk.repositories;

import com.marczak.helpdesk.domain.PersonalData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonalDataRepository extends JpaRepository<PersonalData, Long> {
}
