package com.marczak.helpdesk.repositories;

import com.marczak.helpdesk.domain.TicketType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TicketTypeRepository extends JpaRepository<TicketType, String> {
}
