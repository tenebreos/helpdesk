package com.marczak.helpdesk.repositories;

import com.marczak.helpdesk.domain.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    // TODO: rewrite the query to return count() instead of objects.
    @Query("select c from Customer as c where " +
            "c.personalData.name like concat('%', :phrase, '%') or " +
            "c.personalData.surname like concat('%', :phrase, '%') or " +
            "c.personalData.email like concat('%', :phrase, '%')")
    Page<Customer> findByPhrase(String phrase, Pageable pageable);
}
