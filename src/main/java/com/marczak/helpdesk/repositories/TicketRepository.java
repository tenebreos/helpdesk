package com.marczak.helpdesk.repositories;

import com.marczak.helpdesk.domain.Customer;
import com.marczak.helpdesk.domain.Product;
import com.marczak.helpdesk.domain.Technician;
import com.marczak.helpdesk.domain.Ticket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.List;

public interface TicketRepository extends JpaRepository<Ticket, Long> {

    // TODO: Test the correctness of the queries.

    @Query("select t from Ticket as t where " +
            "t.customer.personalData.name like concat('%', :phrase, '%') or " +
            "t.customer.personalData.surname like concat('%', :phrase, '%') or " +
            "t.technician.personalData.name like concat('%', :phrase, '%') or " +
            "t.technician.personalData.surname like concat('%', :phrase, '%') or " +
            "t.type.name like concat('%', :phrase, '%') or " +
            "t.state like concat('%', :phrase, '%') or " +
            "t.product.name like concat('%', :phrase, '%') or " +
            "t.description like concat('%', :phrase, '%')"
    )
    Page<Ticket> findBySearchPhrase(String phrase, Pageable pageable);

    Page<Ticket> findByTechnician(Technician technician, Pageable pageable);

    @Query("select t from Ticket as t where " +
            "t.technician = :technician and (" +
            "t.customer.personalData.name like concat('%', :phrase, '%') or " +
            "t.customer.personalData.surname like concat('%', :phrase, '%') or " +
            "t.product.name like concat('%', :phrase, '%') or " +
            "t.description like concat('%', :phrase, '%') or " +
            "t.type.name like concat('%', :phrase, '%') or " +
            "t.state like concat('%', :phrase, '%'))")
    Page<Ticket> findByPhraseAndTechnician(String phrase, Technician technician, Pageable pageable);

    Page<Ticket> findByCustomer(Customer customer, Pageable pageable);

    // TODO: rewrite the query to return count() instead of objects.
    List<Ticket> findByCustomer(Customer customer);

    @Query("select t from Ticket as t where " +
            "t.customer = :customer and (" +
            "t.technician.personalData.name like concat('%', :phrase, '%') or " +
            "t.technician.personalData.surname like concat('%', :phrase, '%') or " +
            "t.product.name like concat('%', :phrase, '%') or " +
            "t.description like concat('%', :phrase, '%') or " +
            "t.type.name like concat('%', :phrase, '%') or " +
            "t.state like concat('%', :phrase, '%'))")
    Page<Ticket> findByPhraseAndCustomer(String phrase, Customer customer, Pageable pageable);

    @Query("select t from Ticket as t where " +
            "t.state = :state and (" +
            "t.technician.personalData.name like concat('%', :phrase, '%') or " +
            "t.technician.personalData.surname like concat('%', :phrase, '%') or " +
            "t.customer.personalData.name like concat('%', :phrase, '%') or " +
            "t.customer.personalData.surname like concat('%', :phrase, '%') or " +
            "t.product.name like concat('%', :phrase, '%') or " +
            "t.type.name like concat('%', :phrase, '%') or " +
            "t.state like concat('%', :phrase, '%'))")
    Page<Ticket> findByStateAndPhrase(String phrase, Ticket.State state, Pageable pageable);

    Page<Ticket> findByState(Ticket.State state, Pageable pageable);

    // TODO: rewrite the query to return count() instead of objects.
    List<Ticket> findByTechnicianAndState(Technician technician, Ticket.State state);

    List<Ticket> findByTechnicianAndCustomer(Technician technician, Customer customer);
}
