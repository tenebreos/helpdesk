package com.marczak.helpdesk.repositories;

import com.marczak.helpdesk.domain.Technician;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface TechnicianRepository extends JpaRepository<Technician, Long> {
    @Query("select t from Technician as t where " +
            "t.bankAccountNumber like concat('%', :phrase, '%') or " +
            "t.personalData.name like concat('%', :phrase, '%') or " +
            "t.personalData.surname like concat('%', :phrase, '%') or " +
            "t.personalData.email like concat('%', :phrase, '%')")
    Page<Technician> findByPhrase(String phrase, Pageable pageable);
}
